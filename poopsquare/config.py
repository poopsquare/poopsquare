import os

from dotenv import Dotenv

try:
    env = Dotenv('./.env')
except IOError:
    env = os.environ

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'DB_SECRET_KEY'


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = env['DATABASE_URL']

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = env['TEST_DB_URL']
