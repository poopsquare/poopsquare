import os
from importlib import import_module

from flask import Flask

from poopsquare.extensions import db
from poopsquare.blueprints import blueprints

def create_app(config_obj):
    app = Flask(__name__)
    app.config.from_object(config_obj)
    db.init_app(app)

    for bp in blueprints:
        import_module(bp.import_name)
        app.register_blueprint(bp)
    return app
