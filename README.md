# Poopsquare API


This is the Poopsquare API.

Requirements:
- Python 3.5

- Flask

- virtualenv and pip

- Auth0 app credentials

- PostgreSQL 9.6



# Running the example

You also need to set the client secret and ID of your Auth0 app as environment variables with the following names respectively: `AUTH0_CLIENT_SECRET` and `AUTH0_CLIENT_ID`.

For that, if you just create a file named `.env` in the directory and set the values like the following, the app will just work:

```bash
# .env file
AUTH0_CLIENT_SECRET=YOUR_CLIENT_SECRET
AUTH0_CLIENT_ID=YOUR_CLIENT_ID
FLASK_ENV="config.DevelopmentConfig"
DATABASE_URL="postgresql://localhost/poopsquare_dev"
```

Once you've set those 2 enviroment variables:

1. Install the needed dependencies with `pip install -r requirements.txt`
2. Create database poopsquare_dev if it does not exist(or set DATABASE_URL environment variable to corresponding database)
3. Start the server with `python manage.py devserver`

## Useful tips

export the flask app to run with  `flask run`:

export FLASK_APP=manage.py

# Running tests

python -m unittest

