from test.setup import TestCase

class HomeTest(TestCase):

    def test_ping(self):
        rv = self.client.get('/ping')
        assert b"All good. You don't need to be authenticated to call this" in rv.data
