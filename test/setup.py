import os
from poopsquare.application import create_app
import unittest
import tempfile

class TestCase(unittest.TestCase):

    def setUp(self):
        poopsquare = create_app('poopsquare.config.TestingConfig')
        self.db_fd, poopsquare.config['TEST_DB_URL'] = tempfile.mkstemp()
        self.app = poopsquare
        self.client = poopsquare.test_client()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(self.app.config['TEST_DB_URL'])
