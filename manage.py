import os

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from poopsquare.application import create_app
from poopsquare.extensions import db

app = create_app('poopsquare.config.DevelopmentConfig')
manager = Manager(app)

@manager.command
def devserver():
    app.run(host='0.0.0.0', port=os.environ.get('PORT', 3001))

@manager.command
def migrate():
    migrate = Migrate(app, db)
    manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
